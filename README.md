# WorkClock
An Android clock  [下载](#直接下载)

<img src="/screenshot/demo.gif" width="768" height="382"/>

## 功能

### 普通状态

<img src="/screenshot/device-base.png" width="768" height="382"/>

  左滑：改变背景动画
  右滑：改变字本
  上滑：增加亮度
  下滑：减小亮度
  双击：界面(简单-详细-全屏)

  定时器  单击->改变状态，长按-> 设置时间

  定时显  单击->修改本次计时时间，不保存  长按->隐去自已
  
  计时显  单击->进入计时模式  长按->变更为定时器倒计时

  日期   单击->显示日历  长按->[切换界面样式](#自定界面样式示例)

  星期   单击->切换走秒方式，长按->切换12或24小时制

  天气   单击->显示近几天天气

  下面说明条 单击->随机名言

  时间  长按-->进入[定制设置]

  锁    单击->锁定屏幕

###  定时提醒状态

  倒计时  单击->结束闹铃，返回[普通状态](#普通状态)
 
 （注：未手动 则闹铃结束后进入计时模式）

###  详细状态

<img src="/screenshot/device-adv.png" width="768" height="382"/>

   会出现颜色选择，及设置按键

   实心马 单击->设置背景色

   空心马1 单击->改变当前的前景色  长按-->设置1号位的前景色

   空心马2 单击->改变当前的前景色  长按-->设置2号位的前景色
   
   图片  单击->背景图显隐  长按 -->重新选择背景图

   时钟  单击->数字/模似切换

   设置  单击->进入[系统设置]()

###  全屏精简模式

<img src="/screenshot/device-full.png" width="768" height="382"/>

  定时显  长按->隐去自已

   左滑：改变背景动画
   右滑：改变字本


###  模拟时钟模式

<img src="/screenshot/device-full-clock.png" width="768" height="382"/>

   时钟  长按-->进入[定制设置]

   左滑：改变背景动画

   右滑：改变样式

   双击：界面(详细-全屏)


6.0系统以下，可以用 近身感应锁屏。 在屏锁状态下无效

### 移除应用:
设置->Uninstall

### 直接下载：

[城通网盘](https://u19673462.ctfile.com/fs/19673462-388092692)

[百度网盘](https://pan.baidu.com/s/1Uc8hdapVkAG1Zj8rFBsYfQ) 提取码: xfsj

[Download APK](https://gitee.com/51danju/workclock/raw/master/workclock20190717.apk)

SourceCode:[WorkClock](https://gitee.com/51danju/workclock)

