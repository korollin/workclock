package com.xenione.digit;

public abstract class AbstractTabAnimation {

    public static final float DEFAULT_ELAPSED_TIME=1000f;

    protected final static int LOWER_POSITION = 0;
    protected final static int MIDDLE_POSITION = 1;
    protected final static int UPPER_POSITION = 2;

    protected final TabDigitEntity mTopTab;
    protected final TabDigitEntity mBottomTab;
    protected final TabDigitEntity mMiddleTab;

    protected int state;
    protected int mAlpha = 0;
    protected long mTime = -1;
    protected float mElapsedTime = 950.0f;

    public AbstractTabAnimation(TabDigitEntity mTopTab, TabDigitEntity mBottomTab, TabDigitEntity mMiddleTab) {
        this.mTopTab = mTopTab;
        this.mBottomTab = mBottomTab;
        this.mMiddleTab = mMiddleTab;
        initState();
    }

    public void start() {
        makeSureCycleIsClosed();
        mTime = System.currentTimeMillis();
    }

    public void sync() {
        makeSureCycleIsClosed();
    }

    public abstract void initState();
    public abstract void initMiddleTab();
    public abstract void run();
    protected abstract void makeSureCycleIsClosed();

    public boolean isRunning(){
        return mTime!=-1;
    }

    public float getmElapsedTime() {
        return mElapsedTime;
    }

    public void setmElapsedTime(float mElapsedTime) {
        this.mElapsedTime = mElapsedTime;
    }
}
