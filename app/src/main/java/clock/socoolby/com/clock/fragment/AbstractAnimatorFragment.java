package clock.socoolby.com.clock.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;
import clock.socoolby.com.clock.widget.animatorview.AnimatorView;
import clock.socoolby.com.clock.widget.animatorview.I_Animator;

public abstract class AbstractAnimatorFragment extends Fragment {

    protected GlobalViewModel globalViewModel;

    protected AnimatorView animatorView;

    protected I_Animator animator;

    public AbstractAnimatorFragment(I_Animator animator) {
        this.animator = animator;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalViewModel= ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(GlobalViewModel.class);
    }


    protected void initAnimatorView(AnimatorView animatorView){
        this.animatorView=animatorView;
        animator.init(getActivity(),animatorView);
        animatorView.setAnimator(animator);
    }

    @Override
    public void onResume() {
        super.onResume();
        animatorView.start();
    }


    @Override
    public void onPause() {
        super.onPause();
        animatorView.pause();
    }

    protected abstract void bindViewModel();

}
