package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

import android.graphics.Canvas;
import android.graphics.Path;

public class LeafTwoPointer extends SecondTailPointer {

    @Override
    public void initPointerLength(float mRadius) {
        super.initPointerLength(mRadius);
        mMinutePointerWidth=mMinutePointerWidth/3;
    }

    @Override
    protected void drawHourPointer(Canvas canvas) {
        mPointerPaint.setStrokeWidth(mHourPointerWidth);
        mPointerPaint.setColor(mColorHourPointer);

        // 当前时间的总秒数
        float s = mH * 60 * 60 + mM * 60 + mS;
        // 百分比
        float percentage = s / (12 * 60 * 60);
        // 通过角度计算弧度值，因为时钟的角度起线是y轴负方向，而View角度的起线是x轴正方向，所以要加270度
        float angle = calcAngle(percentage);

        float x = calcX(mHourPointerLength, angle);
        float y = calcY(mHourPointerLength, angle);

        float minLength = mHourPointerLength / 2;

        float x1 = calcX(minLength, angle + 20);
        float y1 = calcY(minLength, angle + 20);

        float x2 = calcX(minLength, angle - 20);
        float y2 = calcY(minLength, angle - 20);


        Path path = new Path();
        path.cubicTo(0, 0, x1, y1, x, y);
        path.cubicTo(0, 0, x2, y2,x, y );

        canvas.drawPath(path, mPointerPaint);
    }

    @Override
    protected void drawMinutePointer(Canvas canvas) {
        mPointerPaint.setStrokeWidth(mMinutePointerWidth);
        mPointerPaint.setColor(mColorMinutePointer);

        float s = mM * 60 + mS;
        float percentage = s / (60 * 60);
        float angle = calcAngle(percentage);

        float x = calcX(mMinutePointerLength, angle);
        float y = calcY(mMinutePointerLength, angle);

        float minLength = mMinutePointerLength / 2;

        float x1 = calcX(minLength, angle + 10);
        float y1 = calcY(minLength, angle + 10);

        float x2 = calcX(minLength, angle - 10);
        float y2 = calcY(minLength, angle - 10);


        Path path = new Path();
        path.cubicTo(0, 0, x1, y1, x, y);
        path.cubicTo(0, 0,x2, y2, x, y);

        canvas.drawPath(path, mPointerPaint);
    }

    public static final String TYPE_LEAF_TWO="leafTwo";


    @Override
    public String typeName() {
        return TYPE_LEAF_TWO;
    }

}