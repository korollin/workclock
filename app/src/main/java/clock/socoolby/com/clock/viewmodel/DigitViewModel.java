package clock.socoolby.com.clock.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import clock.socoolby.com.clock.dao.base.TimeFontStyle;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.widget.textview.ShadowTypeEnum;
import clock.socoolby.com.clock.widget.textview.charanimator.CharAnimatorEnum;

public class DigitViewModel extends ViewModel {

    private MutableLiveData<Boolean> displaySecond=new MutableLiveData<>();

    private MutableLiveData<TimeFontStyle> timeFontStyle =new MutableLiveData<>();

    private MutableLiveData<String> timeText =new MutableLiveData<>();

    private MutableLiveData<Boolean> linearGradientAble=new MutableLiveData<>();

    private MutableLiveData<Integer[]> timeLinearGradientColorsArray=new MutableLiveData<>();

    private MutableLiveData<Boolean> reflectedAble=new MutableLiveData<>();//倒影

    private MutableLiveData<Integer> timeFontStyleSize=new MutableLiveData<>();

    private MutableLiveData<ShadowTypeEnum> shadowType=new MutableLiveData<>();//0:不设置，1：阴影，2:浮雕

    private MutableLiveData<CharAnimatorEnum> timeCharAnimatorType=new MutableLiveData<>();

    private MutableLiveData<Integer> baseLineDown=new MutableLiveData<>();

    private MutableLiveData<String> timeFontName =new MutableLiveData<>();

    private MutableLiveData<Boolean> secondSubscript=new MutableLiveData<>();//缩小

    private MutableLiveData<Boolean> charBackgroundBorder=new MutableLiveData<>();

    private MutableLiveData<Integer> charBackgroundBorderColor=new MutableLiveData<>();

    private MutableLiveData<Integer> charBackgroundBorderDividerColor=new MutableLiveData<>();

    private MutableLiveData<Integer> charBackgroundBorderDividerWidth=new MutableLiveData<>();

    private MutableLiveData<Integer> subscriptFontScale=new MutableLiveData<>();

    private MutableLiveData<Integer> timeTextPadding=new MutableLiveData<>();

    private MutableLiveData<Boolean> charBackgroundBorderWithDoubble=new MutableLiveData<>();

    private MutableLiveData<Integer> baseLineX =new MutableLiveData<>();


    SharePerferenceModel model;

    public DigitViewModel(SharePerferenceModel model) {
        this.model = model;
        loadFromModel();
    }

    public void loadFromModel(){
        displaySecond.setValue(model.isDisplaySecond());
        linearGradientAble.setValue(model.isLinearGradientAble());
        timeLinearGradientColorsArray.setValue(model.getTimeColorsArray());
        reflectedAble.setValue(model.isReflectedAble());
        shadowType.setValue(ShadowTypeEnum.valueOf(model.getShadowType()));
        timeCharAnimatorType.setValue(CharAnimatorEnum.valueOf(model.getTimeTextCharAnimatorType()));
        baseLineDown.setValue(model.getBaseLineDown());
        timeFontStyleSize.setValue(model.getFontStyleSize());
        timeFontName.setValue(model.getFontName());
        secondSubscript.setValue(model.isTimeTextSecondSubscript());
        charBackgroundBorder.setValue(model.isCharBackgroundBorder());
        charBackgroundBorderColor.setValue(model.getCharBackgroundBorderColor());
        charBackgroundBorderDividerColor.setValue(model.getCharBackgroundBorderDividerColor());
        charBackgroundBorderDividerWidth.setValue(model.getCharBackgroundBorderDividerStrokeWidth());
        subscriptFontScale.setValue(model.getSubscriptFontScale());
        timeTextPadding.setValue(model.getTimeTextPadding());
        charBackgroundBorderWithDoubble.setValue(model.isCharBackgroundBorderWithDoubble());
        baseLineX.setValue(model.getBaseLineX());
    }

    public MutableLiveData<Boolean> getDisplaySecond() {
        return displaySecond;
    }

    public MutableLiveData<TimeFontStyle> getTimeFontStyle() {
        return timeFontStyle;
    }

    public MutableLiveData<String> getTimeText() {
        return timeText;
    }

    public MutableLiveData<Boolean> getLinearGradientAble() {
        return linearGradientAble;
    }

    public MutableLiveData<Integer[]> getTimeLinearGradientColorsArray() {
        return timeLinearGradientColorsArray;
    }

    public MutableLiveData<Boolean> getReflectedAble() {
        return reflectedAble;
    }

    public MutableLiveData<Integer> getTimeFontStyleSize() {
        return timeFontStyleSize;
    }

    public MutableLiveData<ShadowTypeEnum> getShadowType() {
        return shadowType;
    }

    public MutableLiveData<CharAnimatorEnum> getTimeCharAnimatorType() {
        return timeCharAnimatorType;
    }

    public MutableLiveData<Integer> getBaseLineDown() {
        return baseLineDown;
    }


    public void setDisplaySecond(Boolean displaySecond) {
        this.displaySecond.setValue(displaySecond);
        model.setDisplaySecond(displaySecond);
    }

    public void setTimeFontStyle(TimeFontStyle timeFontStyle) {
        this.timeFontStyle.setValue(timeFontStyle);
        model.setFontName(timeFontStyle.name);
    }

    public void setTimeText(String timeText) {
        this.timeText.setValue(timeText);
    }

    public void setLinearGradientAble(Boolean linearGradientAble) {
        this.linearGradientAble.setValue(linearGradientAble);
        model.setLinearGradientAble(linearGradientAble);
    }

    public void setTimeLinearGradientColorsArray(Integer[] timeLinearGradientColorsArray) {
        this.timeLinearGradientColorsArray.setValue(timeLinearGradientColorsArray);
        model.setTimeColorsArray(timeLinearGradientColorsArray);
    }

    public void setReflectedAble(Boolean reflectedAble) {
        this.reflectedAble.setValue(reflectedAble);
        model.setReflectedAble(reflectedAble);
    }

    public void setTimeFontStyleSize(Integer timeFontStyleSize) {
        this.timeFontStyleSize.setValue(timeFontStyleSize);
        //model.setFontStyleSize(timeFontStyleSize);
    }

    public void setShadowType(ShadowTypeEnum shadowType) {
        this.shadowType.setValue(shadowType);
        model.setShadowType(shadowType.getTypeCode());
    }

    public void setTimeCharAnimatorType(CharAnimatorEnum timeCharAnimatorType) {
        this.timeCharAnimatorType.setValue(timeCharAnimatorType);
        model.setTimeTextCharAnimatorType(timeCharAnimatorType.getStyleCode());
    }

    public void setBaseLineDown(Integer baseLineDown) {
        this.baseLineDown.setValue(baseLineDown);
        model.setBaseLineDown(baseLineDown);
    }

    public MutableLiveData<String> getTimeFontName() {
        return timeFontName;
    }

    public MutableLiveData<Boolean> getSecondSubscript() {
        return secondSubscript;
    }

    public void setSecondSubscript(Boolean secondSubscript) {
        this.secondSubscript.setValue(secondSubscript);
        model.setTimeTextSecondSubscript(secondSubscript);
    }

    public MutableLiveData<Boolean> getCharBackgroundBorder() {
        return charBackgroundBorder;
    }

    public void setCharBackgroundBorder(Boolean charBackgroundBorder) {
        this.charBackgroundBorder.setValue(charBackgroundBorder);
        model.setCharBackgroundBorder(charBackgroundBorder);
    }

    public MutableLiveData<Integer> getCharBackgroundBorderColor() {
        return charBackgroundBorderColor;
    }

    public void setCharBackgroundBorderColor(Integer charBackgroundBorderColor) {
        this.charBackgroundBorderColor.setValue(charBackgroundBorderColor);
        model.setCharBackgroundBorderColor(charBackgroundBorderColor);
    }

    public MutableLiveData<Integer> getCharBackgroundBorderDividerColor() {
        return charBackgroundBorderDividerColor;
    }

    public void setCharBackgroundBorderDividerColor(Integer charBackgroundBorderDividerColor) {
        this.charBackgroundBorderDividerColor.setValue(charBackgroundBorderDividerColor);
        model.setCharBackgroundBorderDividerColor(charBackgroundBorderDividerColor);
    }

    public MutableLiveData<Integer> getCharBackgroundBorderDividerWidth() {
        return charBackgroundBorderDividerWidth;
    }

    public void setCharBackgroundBorderDividerWidth(Integer charBackgroundBorderDividerWidth) {
        this.charBackgroundBorderDividerWidth.setValue(charBackgroundBorderDividerWidth);
        model.setCharBackgroundBorderDividerStrokeWidth(charBackgroundBorderDividerWidth);
    }

    public MutableLiveData<Integer> getSubscriptFontScale() {
        return subscriptFontScale;
    }

    public void setSubscriptFontScale(Integer subscriptFontScale) {
        this.subscriptFontScale.setValue(subscriptFontScale);
        model.setSubscriptFontScale(subscriptFontScale);
    }

    public MutableLiveData<Integer> getTimeTextPadding() {
        return timeTextPadding;
    }

    public void setTimeTextPadding(Integer timeTextPadding) {
        this.timeTextPadding.setValue(timeTextPadding);
        model.setTimeTextPadding(timeTextPadding);
    }

    public MutableLiveData<Boolean> getCharBackgroundBorderWithDoubble() {
        return charBackgroundBorderWithDoubble;
    }

    public void setCharBackgroundBorderWithDoubble(Boolean charBackgroundBorderWithDoubble) {
        this.charBackgroundBorderWithDoubble.setValue(charBackgroundBorderWithDoubble);
        model.setCharBackgroundBorderWithDoubble(charBackgroundBorderWithDoubble);
    }

    public MutableLiveData<Integer> getBaseLineX() {
        return baseLineX;
    }

    public void setBaseLineX(Integer baseLineX) {
        this.baseLineX.setValue(baseLineX);
        model.setBaseLineX(baseLineX);
    }
}
