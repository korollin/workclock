package clock.socoolby.com.clock.widget.animatorview.animator.model.particle.drawer;

import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.I_PraticleDraw;
import clock.socoolby.com.clock.widget.animatorview.animator.model.particle.Particle;

public class CircleDrawer implements I_PraticleDraw {
    public static final CircleDrawer INSTANCE=new CircleDrawer();

    @Override
    public void draw(Canvas canvas, Particle particle, Paint paint) {
        paint.setAlpha(particle.alpha);
        paint.setColor(particle.color);
        canvas.drawCircle(particle.x,particle.y,particle.r,paint);
    }
}
