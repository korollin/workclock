package clock.socoolby.com.clock.widget.spirit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;

/**
 * Created by QJoy on 2017.12.25.
 */
public  class LibgdxSpineEffectView extends AbstractLibgdxSpineEffectView {

	public static final String NAME="spineboy";

	public LibgdxSpineEffectView() {
		super(true);
	}


	public void setAction(ActionEnum action){
		if (loadingAnimate)
			return;
		if(befAction!=null&&befAction==action)
			return;
		befAction=action;
		switch (action){
			case RUN:
				state.setAnimation(0, "run", true);
				break;
			case WALK:
				state.setAnimation(0, "walk", true);
				break;
			case JUMP:
				state.setAnimation(0, "jump", false);
				state.addAnimation(0, "run", true, 0);
				break;
			case IDLE:
				state.setAnimation(0, "idle", true);
				break;
		}
	}

	@Override
	public FileHandle getExtureFile() {
		return Gdx.files.internal("spine/spineboy-pma.atlas");
	}

	@Override
	public FileHandle getJsonFile() {
		return Gdx.files.internal("spine/spineboy-ess.json");
	}


	@Override
	public void initAnimationStateData(AnimationStateData stateData, AnimationState state) {
		stateData.setMix("run", "jump", 0.2f);
		stateData.setMix("jump", "run", 0.2f);

		stateData.setMix("run","walk",1f);
		stateData.setMix("walk","run",1f);

		state.setTimeScale(1.0f); // Slow all animations down to 50% speed.

		// Queue animations on track 0.
		state.setAnimation(0, "idle", true);
	}
}
