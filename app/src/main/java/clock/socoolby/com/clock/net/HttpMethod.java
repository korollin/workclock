package clock.socoolby.com.clock.net;

import com.android.volley.Request;

/**
 * The http method for a request.
 */
public enum HttpMethod {

    /**
     * Get
     */
    GET(Request.Method.GET),

    /**
     * Post
     */
    POST(Request.Method.POST),

    /**
     * Patch
     */
    PATCH(Request.Method.PATCH),

    /**
     * Delete
     */
    DELETE(Request.Method.DELETE),

    /**
     * Put
     */
    PUT(Request.Method.PUT),
    ;

    public int value;

    HttpMethod(int typeValue) {
        this.value=typeValue;
    }
}