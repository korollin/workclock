package clock.socoolby.com.clock.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {

    public static String[] readStringArray(JSONObject jsonObject,String lable) throws JSONException {
        String array=jsonObject.getString(lable);
        return array.split(",");
    }
}
