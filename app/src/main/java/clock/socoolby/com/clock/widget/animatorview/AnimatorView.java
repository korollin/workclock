package clock.socoolby.com.clock.widget.animatorview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class AnimatorView extends View {

    private Context context;

    private I_Animator animator;
    private int color= Color.BLACK;

    public AnimatorView(Context context) {
        super(context);
        this.context = context;
    }

    public AnimatorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Reset list of droidflakes, then restart it with 8 flakes
        if(animator!=null)
           animator.onSizeChanged(w,h,oldw,oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(animator!=null)
            animator.onDraw(canvas);
    }

    /**
     * 暂停动画
     */
    public void pause() {
        if(animator!=null)
            animator.pause();
    }

    /**
     * 开始动画
     */
    public void start() {
        if(animator!=null)
            animator.start();
    }

    public void stop(){
        if(animator!=null)
            animator.stop();
    }

    public boolean isRunning(){
        if(animator==null){
            return false;
        }
        return animator.isRunning();
    }

    public void setAnimator(I_Animator newAnimator){
        if(animator!=null)
            animator.stop();
        animator=newAnimator;
        if(animator!=null) {
            animator.setColor(color);
            animator.start();
        }else{
            invalidate();
        }
    }

    public void setColor(int color){
        this.color=color;
        if(animator!=null&&animator.isColorUseAble()&&!animator.isRandColor()) {
            animator.setColor(color);
        }
    }
}