package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

//螺旋线
public class HelixClock extends AbstractClock {


    /**
     * 绘制时钟的圆形和刻度
     */
    protected void drawBorder(Canvas canvas) {
       /** canvas.save();
        canvas.translate(mCenterX, mCenterY);
        mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
        mDefaultPaint.setParticleColor(mClockColor);

        canvas.drawCircle(0, 0, mRadius, mDefaultPaint);

        for (int i = 0; i < 60; i++) {
            if (i % 5 == 0) { // 特殊时刻

                mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                mDefaultPaint.setParticleColor(mColorParticularyScale);

                canvas.drawLine(0, -mRadius, 0, -mRadius + mParticularlyScaleLength, mDefaultPaint);

            } else {          // 一般时刻

                mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                mDefaultPaint.setParticleColor(mColorDefaultScale);

                canvas.drawLine(0, -mRadius, 0, -mRadius + mDefaultScaleLength, mDefaultPaint);

            }
            canvas.rotate(6);
        }
        canvas.restore();
        **/
    }


    @Override
    public  String typeName() {
        return TYPE_HELIX;
    }

    public static final String TYPE_HELIX="helix";


}
