package clock.socoolby.com.clock.widget.textview.border;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class DefaultBorder extends AbstractBorder {

    public void drawBackground(Canvas canvas, float startX, float startY, int width, int height, Paint mBackgroundPaint) {
        canvas.drawRoundRect(new RectF(startX, startY, startX + width, startY + height), 10, 10, mBackgroundPaint);
    }
}
