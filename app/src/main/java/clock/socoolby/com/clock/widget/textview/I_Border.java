package clock.socoolby.com.clock.widget.textview;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface I_Border {
      void drawBackground(Canvas canvas, float startX, float startY, int width, int height, Paint mBackgroundPaint);

      void drawDivider(Canvas canvas, float startX, float startY, int width, int height, Paint mDividerPaint);
}
