package clock.socoolby.com.clock.utils;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

public class DialogUtils {

    public interface OkCancelSelectedLinstener {
        void onReturn(boolean ok);
    }

    public static void show(Context context, String title, String message, OkCancelSelectedLinstener linstener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("确定", (dialog, which) -> {
           linstener.onReturn(true);
            dialog.dismiss();
        });
        builder.setNegativeButton("取消", (dialog, which) -> {
            linstener.onReturn(false);
            dialog.dismiss();
        });
        builder.create().show();
    }
}
