package clock.socoolby.com.clock.widget.animatorview.animator.model.speed;


import clock.socoolby.com.clock.widget.animatorview.I_SpeedInterpolator;

public abstract class AbstractInterpolator<T extends Number> implements I_SpeedInterpolator<T> {
    protected T base;

    protected T currentValue;

    protected T maxValue=null;

    protected boolean running=true;

    int runCount=0;

    public AbstractInterpolator(T base, T maxValue) {
        this.base = base;
        this.maxValue = maxValue;
    }

    public void init(){
        currentValue=base;
        runCount=0;
        running=true;
    }


    @Override
    public void speedUp(int percent) {

    }

    @Override
    public void speedDown(int percent) {

    }

    @Override
    public void resetSpeed() {

    }

    @Override
    public void start() {
        running=true;
    }

    @Override
    public void stop() {
        running=false;
    }

    @Override
    public T move() {
        if(running&&!isEnd()){
           currentValue = calc();
           runCount++;
        }
        return currentValue;
    }

    public boolean isEnd(){
        return base.floatValue()>=maxValue.floatValue();
    }

    public abstract T calc();

}
