package clock.socoolby.com.clock;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;

import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.fragment.spine.LibgdxSpineFragment;
import clock.socoolby.com.clock.state.ClockModeEnum;
import clock.socoolby.com.clock.viewmodel.AlterViewModel;
import clock.socoolby.com.clock.viewmodel.DigitViewModel;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.SimulateViewModel;
import clock.socoolby.com.clock.viewmodel.ThemeUIViewModel;
import clock.socoolby.com.clock.widget.layout.DragFrameLayout;
import clock.socoolby.com.clock.widget.spirit.ClockStateEnum;
import clock.socoolby.com.clock.widget.spirit.LibgdxSpineEffectView;

public class SpiritsManager implements DragFrameLayout.OnDragDropListener {
    public static final String TAG=SpiritsManager.class.getName();

    DragFrameLayout layout;

    GlobalViewModel globalViewModel;
    DigitViewModel digitViewModel;
    SimulateViewModel simulateViewModel;
    ThemeUIViewModel themeUIViewModel;
    AlterViewModel alterViewModel;

    FragmentManager fragmentManager;

    LibgdxSpineFragment libgdxSpineFragment;

    private boolean running=false;

    int fragmentContainerId;

    public SpiritsManager(GlobalViewModel globalViewModel, DigitViewModel digitViewModel, SimulateViewModel simulateViewModel, ThemeUIViewModel themeUIViewModel, AlterViewModel alterViewModel) {
        this.globalViewModel = globalViewModel;
        this.digitViewModel = digitViewModel;
        this.simulateViewModel = simulateViewModel;
        this.themeUIViewModel = themeUIViewModel;
        this.alterViewModel = alterViewModel;
        LibgdxSpineFragment.setOpenDEBUGLog(true);
    }

    public void initContiner(DragFrameLayout layout, FragmentManager fragmentManager,int fragmentId){
        this.layout=layout;
        layout.setVisibility(View.GONE);
        layout.setOnDragDropListener(this);
        this.fragmentManager=fragmentManager;
        this.fragmentContainerId=fragmentId;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        if(!globalViewModel.isFullscreenSpiritAble())
            return;
        if(layout==null||this.running==running)
            return;
        this.running = running;
        if(running){
            layout.setVisibility(View.VISIBLE);
            addSpirits();
            libgdxSpineFragment=new LibgdxSpineFragment(LibgdxSpineEffectView.NAME);
            fragmentManager.beginTransaction().replace(fragmentContainerId,libgdxSpineFragment).commit();
            bindViewModel();
        }else{
            layout.setVisibility(View.GONE);
            removeSpirits();
            if(libgdxSpineFragment!=null)
               fragmentManager.beginTransaction().remove(libgdxSpineFragment).commit();
            libgdxSpineFragment=null;
        }
    }

    private void bindViewModel() {
        /*globalViewModel.getTimeHourAnimatorStarting().observe(libgdxSpineFragment, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(libgdxSpineFragment.hasBuilt())
                   libgdxSpineFragment.setClockState(aBoolean ? ClockStateEnum.HOUR_ANIMATION:ClockStateEnum.NORMAL);
            }
        });*/

        EventManger.addHourAnimatorListener(libgdxSpineFragment,event -> {
            if(libgdxSpineFragment.hasBuilt())
                libgdxSpineFragment.setClockState(event.getValue() ? ClockStateEnum.HOUR_ANIMATION:ClockStateEnum.NORMAL);
        });

        globalViewModel.getClockModeEnum().observe(libgdxSpineFragment, new Observer<ClockModeEnum>() {
            @Override
            public void onChanged(ClockModeEnum clockModeEnum) {
                if(!libgdxSpineFragment.hasBuilt())
                    return;
                switch (clockModeEnum){
                    case ALTER:
                    case HANDUP:
                        libgdxSpineFragment.setClockState(ClockStateEnum.HANDUP);
                        break;
                    case NORMAL:
                        libgdxSpineFragment.setClockState(ClockStateEnum.NORMAL);
                        break;
                    case DELAY:
                        libgdxSpineFragment.setClockState(ClockStateEnum.COUNTING);
                        break;
                }
            }
        });

        globalViewModel.getAppConfig().observe(libgdxSpineFragment, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                libgdxSpineFragment.setCanDraw(!aBoolean);
            }
        });
    }

    Button demo;

    private void addSpirits(){
        /*demo=new Button(layout.getContext());
        demo.setText("我是一个小精灵");
        demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timber.log.Timber.d("demo Button clicked");
            }
        });
        layout.addDragChildView(demo,10,20);*/
    }

    private void removeSpirits(){
        /*layout.removeDragChildView(demo);
        demo=null;*/
    }

    @Override
    public void onDragStart(View view) {
        timber.log.Timber.d("onDragStart view:"+view.getId());
    }

    @Override
    public void ontDraging(View view) {
        timber.log.Timber.d("onDraging view:"+view.getId());
    }

    @Override
    public void onDragEnd(View view,int left,int top) {
        timber.log.Timber.d("onDragEnd view is demo:"+(view==demo)+"\t left padding:"+left+"\t top padding:"+top);
    }
}
