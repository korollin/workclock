package clock.socoolby.com.clock.fragment.handup;


import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import android.view.View;
import android.widget.TextView;

import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.event.ClockEvent;
import clock.socoolby.com.clock.event.ClockEventListener;
import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.state.ClockStateMachine;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HandUpDefaultFragment} factory method to
 * create an instance of this fragment.
 */
public class HandUpDefaultFragment extends AbstractHandUpFragment {

    public static final String NAME="default";

    private TextView tv_handup_text;

    private int handUPDialy=0;


    public HandUpDefaultFragment() {
        super(R.layout.fragment_handup);
    }

    @Override
    void bindView(View rootView) {
        tv_handup_text=rootView.findViewById(R.id.tv_handup_image);
        tv_handup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endHandUp(true);
            }
        });
    }

    void bindViewModel(){
        alterViewModel.getHandUPDialy().observe(this, new Observer<Integer>() {
                    @Override
                    public void onChanged(Integer integer) {
                        handUPDialy=integer;
                    }
         });

         /*globalViewModel.getHeartbeat().observe(this, new Observer<Boolean>() {
             @Override
             public void onChanged(Boolean aBoolean) {
                 handUPDialy--;
                 tv_handup_text.setText("hand up ! \t"+handUPDialy);
                 if(handUPDialy==0)
                     endHandUp(false);
                 else
                     playHandUpMusic();
             }
         });*/

        EventManger.addHeartbeatListener(this, new ClockEventListener<Boolean>() {
            @Override
            public void onEvent(ClockEvent<Boolean> event) {
                handUPDialy--;
                tv_handup_text.setText("hand up ! \t"+handUPDialy);
                if(handUPDialy==0)
                    endHandUp(false);
                else
                    playHandUpMusic();
            }
        });

         globalViewModel.getForegroundColor().observe(this, new Observer<Integer>() {
             @Override
             public void onChanged(Integer integer) {
                 tv_handup_text.setTextColor(integer);
             }
         });
    }
}
