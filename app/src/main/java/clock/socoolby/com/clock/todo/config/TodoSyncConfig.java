package clock.socoolby.com.clock.todo.config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import clock.socoolby.com.clock.net.base.I_JsonObject;
import clock.socoolby.com.clock.todo.microsoft.MicrosoftTodoSyncConfigEntity;
import clock.socoolby.com.clock.todo.microsoft.MicrosoftTodoSyncServiceImpl;

public class TodoSyncConfig implements I_JsonObject{
    public static final String JSON_TAG="sync_config";

    public static final String SYNC_DELAY_SECOND="sync_delay_second";
    int delaySeconds=5000;

    public static final String SYNC_NO_COMPLETE_ONLY="sync_no_complete_only";
    boolean noCompleteOnly =true;

    public static final String SYNC_USE_SYSTEM_ALTER_MANAGER="sync_use_system_alter_manager";
    boolean useSystemAlterManager=false;

    Map<String,AbstractTodoSyncConfigEntity> todoSyncConfigEntityList=new HashMap<>();

    public TodoSyncConfig() {
        todoSyncConfigEntityList.put(MicrosoftTodoSyncServiceImpl.NAME,getConfigEntityByName(MicrosoftTodoSyncServiceImpl.NAME));
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        delaySeconds=jsonObject.optInt(SYNC_DELAY_SECOND,5000);
        noCompleteOnly =jsonObject.optBoolean(SYNC_NO_COMPLETE_ONLY,true);

        JSONArray jsonArray=jsonObject.getJSONArray(JSON_TAG);
        JSONObject object;
        String serverName;
        AbstractTodoSyncConfigEntity configEntity;
        for(int i=0;i<jsonArray.length();i++){
            object=jsonArray.getJSONObject(i);
            serverName=object.getString(AbstractTodoSyncConfigEntity.SERVICENAME);
            configEntity=getConfigEntityByName(serverName);
            if(configEntity!=null) {
                configEntity.fromJson(object);
                todoSyncConfigEntityList.put(serverName,configEntity);
            }
        }
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {
        jsonObject.put(SYNC_DELAY_SECOND,delaySeconds);
        jsonObject.put(SYNC_NO_COMPLETE_ONLY, noCompleteOnly);

        JSONArray jsonArray=new JSONArray();
        JSONObject object;
        for(AbstractTodoSyncConfigEntity entity:todoSyncConfigEntityList.values()){
            object=new JSONObject();
            entity.toJson(object);
            jsonArray.put(object);
        }
        jsonObject.put(JSON_TAG,jsonArray);
    }

    private AbstractTodoSyncConfigEntity getConfigEntityByName(String name){
        switch (name){
            case MicrosoftTodoSyncServiceImpl.NAME:
               return new MicrosoftTodoSyncConfigEntity();
        }
        return null;
    }

    public int getDelaySeconds() {
        return delaySeconds;
    }

    public void setDelaySeconds(int delaySeconds) {
        this.delaySeconds = delaySeconds;
    }

    public boolean isNoCompleteOnly() {
        return noCompleteOnly;
    }

    public void setNoCompleteOnly(boolean noCompleteOnly) {
        this.noCompleteOnly = noCompleteOnly;
    }
}
