package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

public class PointerFactory {

    public static AbstractPointer build(String name){
        AbstractPointer pointer=null;
        switch (name){
            case LeafPointer.TYPE_LEAF:
                pointer=new LeafPointer();
                break;
            case LeafTwoPointer.TYPE_LEAF_TWO:
                pointer=new LeafTwoPointer();
                break;
            case SecondTailPointer.TYPE_SECOND_TAIL:
                pointer=new SecondTailPointer();
                break;
            case SwordPointer.TYPE_SWORD_POINTER:
                pointer=new SwordPointer();
                break;
            case TrianglePointer.TYPE_TRIANGLE_POINTER:
                pointer=new TrianglePointer();
                break;
            case TwoStepPointer.TYPE_TWO_STEP_POINTER:
                pointer=new TwoStepPointer();
                break;
            default:
                pointer= new DefaultPointer();

        }
        return pointer;
    }
}
