package clock.socoolby.com.clock.alter;

public enum WorkRuleLoopType {
    DateTime(0),EventDay(0xff),Mod(0x01),Tur(0x02);

    int loopType;

    WorkRuleLoopType(int loopType) {
        this.loopType = loopType;
    }
}
