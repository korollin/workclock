package clock.socoolby.com.clock.widget.animatorview;

public abstract class AbstractSuperControlAnimatorEntry implements I_AnimatorEntry {

    @Override
    public void move(int maxWidth, int maxHight) {
        throw new RuntimeException("this animator entry move is super controled,please check use logic....");
    }
}
