package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.List;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//引用自：http://www.gcssloop.com/gebug/bubble-sample

public class BubbleAnimator extends AbstractAnimator<BubbleAnimator.Bubble> {

    public static final String NAME="Bubble";

    private int mBubbleMaxRadius = 30;          // 气泡最大半径 px
    private int mBubbleMinRadius = 5;           // 气泡最小半径 px
    private int mBubbleMaxSize = 30;            // 气泡数量
    private int mBubbleRefreshTime = 20;        // 刷新间隔
    private int mBubbleMaxSpeedY = 5;           // 气泡速度
    private int mBubbleAlpha = 128;             // 气泡画笔

    private RectF mContentRectF;                // 实际可用内容区域

    //private Paint mBubblePaint;                 // 气泡画笔

    public BubbleAnimator() {
        super(DYNAMIC_QUANTITY);
    }

    @Override
    public boolean run() {
        tryCreateBubble();
        refreshBubbles();
        return true;
    }

    @Override
    public void onDraw(Canvas canvas) {
        for (Bubble bubble : list) {
            if (null == bubble) continue;
           bubble.onDraw(canvas,mPaint);
        }
    }

    @Override
    public Bubble createNewEntry() {
        Bubble bubble = new Bubble();
        int radius = rand.nextInt(mBubbleMaxRadius - mBubbleMinRadius);
        radius += mBubbleMinRadius;
        float speedY = rand.nextFloat() * mBubbleMaxSpeedY;
        while (speedY < 1) {
            speedY = rand.nextFloat() * mBubbleMaxSpeedY;
        }
        bubble.radius = radius;
        bubble.speedY = speedY;
        switch (rand.nextInt(10)) {
            case 1:
                bubble.x = mContentRectF.centerX()/2;
                break;
            case 2:
                bubble.x =mContentRectF.centerX()+mContentRectF.centerX()/2;
                break;
            case 3:
                bubble.x =rand.nextInt(Float.valueOf(mContentRectF.width()).intValue());
                break;
            default:
                bubble.x = mContentRectF.centerX();
        }
        bubble.y = mContentRectF.bottom - radius;
        float speedX = rand.nextFloat() - 0.5f;
        while (speedX == 0) {
            speedX = rand.nextFloat() - 0.5f;
        }
        bubble.speedX = speedX * 2;
        randomColorIfAble();
        bubble.color=color;
        return bubble;
    }


    @Override
    protected void initPaint(Paint mPaint) {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(mBubbleAlpha);
        mContentRectF=new RectF(0,0,width,height);
    }

    //--- 气泡效果 ---------------------------------------------------------------------------------

    public class Bubble implements I_AnimatorEntry {
        int radius;     // 气泡半径
        float speedY;   // 上升速度
        float speedX;   // 平移速度
        float x;        // 气泡x坐标
        float y;        // 气泡y坐标
        int color;      // 气泡颜色

        @Override
        public void move(int maxWidth, int maxHight) {

        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            mPaint.setColor(color);
            canvas.drawCircle(x, y,radius, mPaint);
        }

        @Override
        public void setAnimatorEntryColor(int color) {
            this.color=color;
        }
    }

    // 尝试创建气泡
    private void tryCreateBubble() {
        if (null == mContentRectF) return;
        if (list.size() >= mBubbleMaxSize) {
            return;
        }
        if (rand.nextFloat() < 0.95) {
            return;
        }
        list.add(createNewEntry());
    }

    // 刷新气泡位置，对于超出区域的气泡进行移除
    private void refreshBubbles() {
        List<Bubble> list = new ArrayList<>(this.list);
        for (Bubble bubble : list) {
            if (bubble.y - bubble.speedY <= mContentRectF.top + bubble.radius) {
                this.list.remove(bubble);
            } else {
                int i = this.list.indexOf(bubble);
                if (bubble.x + bubble.speedX <= mContentRectF.left + bubble.radius) {
                    bubble.x = mContentRectF.left + bubble.radius ;
                } else if (bubble.x + bubble.speedX >= mContentRectF.right - bubble.radius ) {
                    bubble.x = mContentRectF.right - bubble.radius;
                } else {
                    bubble.x = bubble.x + bubble.speedX;
                }
                bubble.y = bubble.y - bubble.speedY;
                this.list.set(i, bubble);
            }
        }
    }
}
