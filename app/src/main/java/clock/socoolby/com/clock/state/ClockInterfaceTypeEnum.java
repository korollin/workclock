package clock.socoolby.com.clock.state;

public enum ClockInterfaceTypeEnum {
    Digit(0),Simulate(1);

    public int code;

    ClockInterfaceTypeEnum(int code) {
        this.code = code;
    }

    public static ClockInterfaceTypeEnum valueOf(int code){
        switch (code){
            case 1:
                return Simulate;
        }
        return Digit;
    }


}
