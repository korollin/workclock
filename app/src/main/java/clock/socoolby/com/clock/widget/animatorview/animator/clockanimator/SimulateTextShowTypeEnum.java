package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

public enum SimulateTextShowTypeEnum {
    FULL(0),FOUR(1),HIDD(2);

    public int code;

    SimulateTextShowTypeEnum(int code) {
        this.code = code;
    }

    public static SimulateTextShowTypeEnum valueOf(int code){
        switch (code){
            case 1:
                return FOUR;
            case 2:
                return HIDD;
            default:
                return FULL;
        }
    }
}
