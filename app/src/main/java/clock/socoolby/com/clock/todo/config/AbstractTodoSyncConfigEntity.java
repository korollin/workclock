package clock.socoolby.com.clock.todo.config;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public  abstract class AbstractTodoSyncConfigEntity implements I_JsonObject {
     public static final String SERVICENAME="serviceName";

     protected  String serviceName="";
     protected  boolean syncAble=false;


    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        serviceName=jsonObject.optString("serviceName",getServiceName());
        syncAble=jsonObject.optBoolean("syncAble",false);
    }

    protected abstract String getServiceName();

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {
        jsonObject.put("serviceName",serviceName);
        jsonObject.put("syncAble",syncAble);
    }
}

