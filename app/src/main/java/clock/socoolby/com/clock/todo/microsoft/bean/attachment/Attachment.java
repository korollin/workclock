package clock.socoolby.com.clock.todo.microsoft.bean.attachment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import clock.socoolby.com.clock.net.base.I_JsonObject;
import clock.socoolby.com.clock.todo.microsoft.bean.AbstractMircsoftEntity;

/**
 *
 * AttachmentContentType	String	MIME 类型。
 * id	String	只读。
 * isInline	Boolean	如果附件是内嵌附件，则为 true；否则为 false。
 * lastModifiedDateTime	DateTimeOffset	时间戳类型表示使用 ISO 8601 格式的日期和时间信息，并且始终处于 UTC 时间。例如，2014 年 1 月 1 日午夜 UTC 类似于如下形式：'2014-01-01T00:00:00Z'
 * 名称	字符串	附件的显示名称。 这不必是实际的文件名。
 * size	Int32	附件大小，以字节为单位。
 */


public class Attachment extends AbstractMircsoftEntity implements I_JsonObject {
    AttachmentContentType contentType;
    Boolean isInline;
    Date lastModifiedDateTime;
    String name;
    Integer size;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        super.fromJson(jsonObject);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
