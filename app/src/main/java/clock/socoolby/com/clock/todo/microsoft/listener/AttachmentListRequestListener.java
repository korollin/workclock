package clock.socoolby.com.clock.todo.microsoft.listener;

import java.util.List;

import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.net.listener.StateAbleRequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.attachment.Attachment;

public class AttachmentListRequestListener extends AbstractMicrosoftEntityListRequestListener<Attachment> {
    public AttachmentListRequestListener(StateAbleRequestListener<List<Attachment>, I_ResponseState> warpListener) {
        super(warpListener);
    }

    @Override
    public Attachment creatEntity() {
        return new Attachment();
    }
}
