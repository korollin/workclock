package clock.socoolby.com.clock.widget.textview.charanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

public  class Down2UpCharAnimator extends AbstractCharAnimator {
    public Down2UpCharAnimator(String preString, String currentString) {
        super(preString, currentString);
    }

    public Down2UpCharAnimator(String preString, String currentString, float runSpeed) {
        super(preString, currentString, runSpeed);
    }

    @Override
    public void drawCharPre(Canvas canvas, String strToDraw, float startX, float startY, Paint mTextPaint, float percent) {
        canvas.drawText(strToDraw, startX, startY-startY*percent, mTextPaint);
    }

    @Override
    public void drawCharCurrent(Canvas canvas, String strToDraw, float startX, float startY, Paint mTextPaint, float percent) {
        canvas.drawText(strToDraw, startX, 2*startY-startY*percent, mTextPaint);
    }

}
