package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//参考自:https://github.com/scwang90/MultiWaveHeader
public class WaterAnimator extends AbstractAnimator<WaterAnimator.Water> {

    public static final String NAME="Water";

    public WaterAnimator() {
        this(1);
    }

    public WaterAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    @Override
    public Water createNewEntry() {
        //randomColorIfAble();
        return new Water(0,0,width,height,height*3/4);
    }


    class Water implements I_AnimatorEntry {
        private Path mAbovePath, mBelowWavePath;
        private Paint mAboveWavePaint;
        //private Paint mBelowWavePaint;

        private DrawFilter mDrawFilter;

        private float φ;
        private double ω;
        private float y, y2;

        private int width,higth,left,top;

        public Water(int left,int top,int width, int higth,int y2) {
            this.width = width;
            this.higth = higth;
            this.left=left;
            this.top=top;
            this.y2=y2;
            init();
        }

        protected void init() {

            //初始化路径
            mAbovePath = new Path();
            mBelowWavePath = new Path();
            //初始化画笔
            //上方波浪
            mAboveWavePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mAboveWavePaint.setAntiAlias(true);
            mAboveWavePaint.setStyle(Paint.Style.FILL);
            mAboveWavePaint.setColor(Color.BLUE);
            //下方波浪
            /*mBelowWavePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mBelowWavePaint.setAntiAlias(true);
            mBelowWavePaint.setStyle(Paint.Style.FILL);
            mBelowWavePaint.setParticleColor(Color.BLUE);
            mBelowWavePaint.setAlpha(60);*/
            //画布抗锯齿
            mDrawFilter = new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);

        }


        @Override
        public void move(int maxWidth, int maxHight) {
            φ -= 0.1f;
            ω = 2 * Math.PI / width;
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            canvas.setDrawFilter(mDrawFilter);

            mAbovePath.reset();
            mBelowWavePath.reset();

            mAbovePath.moveTo(left, higth);
//        mBelowWavePath.moveTo(getLeft(), getBottom() + 15);
            for (float x = 0; x <= width; x++) {
                /**
                 *  y=Asin(ωx+φ)+k
                 *  A—振幅越大，波形在y轴上最大与最小值的差值越大
                 *  ω—角速度， 控制正弦周期(单位角度内震动的次数)
                 *  φ—初相，反映在坐标系上则为图像的左右移动。这里通过不断改变φ,达到波浪移动效果
                 *  k—偏距，反映在坐标系上则为图像的上移或下移。
                 */
                y = (float) (30 * Math.cos(ω * x + φ) + 30)+y2;
//            y2 = (float) (30 * Math.sin(ω * percent + φ) + 30);
                mAbovePath.lineTo(x, y);
//            mBelowWavePath.lineTo(percent, y2);
                if (x == width / 2) {
                    mPaint.setColor(color);
                    canvas.drawCircle(x, y-40, 20, mPaint);
                    //mWaveAnimationListener.OnWaveAnimation(y);
                }
            }
            //回调 把y坐标的值传出去(在activity里面接收让小机器人随波浪一起摇摆)
            mAbovePath.lineTo(width, higth);
//        mBelowWavePath.lineTo(getRight(), getBottom() + 15);
            canvas.drawPath(mAbovePath, mAboveWavePaint);
//        canvas.drawPath(mBelowWavePath, mBelowWavePaint);
        }

        @Override
        public void setAnimatorEntryColor(int color) {
            mAboveWavePaint.setColor(color);
        }
    }

}
