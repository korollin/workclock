package clock.socoolby.com.clock.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.graphics.Color;
import android.net.Uri;

import java.util.Date;

import clock.socoolby.com.clock.ThemeUIManager;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.state.ClockModeEnum;
import clock.socoolby.com.clock.state.ClockInterfaceTypeEnum;
import clock.socoolby.com.clock.state.ClockStateMachine;
import clock.socoolby.com.clock.todo.TodoSyncServiceManager;

public class GlobalViewModel extends ViewModel {

    //ui
    private MutableLiveData<Integer> backgroundColor=new MutableLiveData<>();

    private MutableLiveData<Integer> foregroundColor=new MutableLiveData<>();

    private MutableLiveData<Uri>     backgroundImageUri=new MutableLiveData<>();

    private MutableLiveData<String>  backgroundAnimatorName=new MutableLiveData<>();

    private MutableLiveData<Integer> backgroundAnimatorColor=new MutableLiveData<>();

    private Boolean                  backgroundAnimatorRandomColor=false;

    private MutableLiveData<ClockInterfaceTypeEnum> clockInterfaceTypeEnum =new MutableLiveData<>();

    private MutableLiveData<Boolean> srceenLock=new MutableLiveData<>();

    private MutableLiveData<Boolean> backgroundImageVisable=new MutableLiveData<>();

    private MutableLiveData<Boolean> autoFullScreen=new MutableLiveData<>();

    private MutableLiveData<Boolean> useSystemWallpaper=new MutableLiveData<>();

    //system
    private Boolean tickSound=false;

    private MutableLiveData<Boolean> appConfig=new MutableLiveData<>();

    private MutableLiveData<String>  mCity=new MutableLiveData<>();

    private MutableLiveData<Integer> brightness=new MutableLiveData<>();

    public  MutableLiveData<Boolean> triggerScreen=new MutableLiveData();

    public  MutableLiveData<Boolean> todoSyncAble=new MutableLiveData();


    //alter handUp
    private MutableLiveData<Integer> handUpTime=new MutableLiveData<>();

    private MutableLiveData<Boolean> handUpAble=new MutableLiveData<>();

    private MutableLiveData<String>  handUpTypeName=new MutableLiveData<>();

    private MutableLiveData<String>  hourAlterTypeName=new MutableLiveData<>();

    //private MutableLiveData<Boolean> timeHourAnimatorStarting =new MutableLiveData<>();

    private MutableLiveData<Integer> timeHourAnimatordialy=new MutableLiveData<>();


    //state
    private MutableLiveData<ClockModeEnum> clockModeEnum=new MutableLiveData<>();

    private MutableLiveData<Boolean> hourSystem12=new MutableLiveData<>();

    private MutableLiveData<Integer> time_hour=new MutableLiveData<>();

    private MutableLiveData<Date>    time_day=new MutableLiveData<>();

    SharePerferenceModel model;

    ThemeUIManager themeUIManager;

    ClockStateMachine clockStateMachine;

    TodoSyncServiceManager todoSyncServiceManager;

    public GlobalViewModel(SharePerferenceModel model) {
        this.model = model;
        init();
        loadFromModel();
    }

    private void init(){
        srceenLock.setValue(false);
        brightness.setValue(100);
        appConfig.setValue(false);
        clockModeEnum.setValue(ClockModeEnum.NORMAL);
    }

    public void loadFromModel(){
        mCity.setValue(model.getCity());
        backgroundColor.setValue(model.getBackgroundColor());
        foregroundColor.setValue(model.getForegroundColor());
        String backgroundImage=model.getBackgroundImage();
        Uri imageUri=backgroundImage.isEmpty()?null:Uri.parse(backgroundImage);
        backgroundImageUri.setValue(imageUri);
        tickSound=model.isTickSound();
        backgroundAnimatorName.setValue(model.getBackgroundAnimatorName());
        backgroundAnimatorColor.setValue(model.getForegroundColor1());
        backgroundAnimatorRandomColor=model.isBackgroundAnimatorRandomColor();
        backgroundImageVisable.setValue(model.isBackgroundImageVisable());

        clockInterfaceTypeEnum.setValue(ClockInterfaceTypeEnum.valueOf(model.getClockInterfaceType()));
        autoFullScreen.setValue(model.isAutoFullscreen());

        todoSyncAble.setValue(model.isTodoSyncAble());

        handUpAble.setValue(model.isHandUpAble());
        handUpTime.setValue(model.getHandUpTime());
        handUpTypeName.setValue(model.getHandUpTypeName());
        hourSystem12.setValue(model.isHourSystem12());
        hourAlterTypeName.setValue(model.getTimeHourAlterTypeName());
        time_hour.setValue(new Date().getHours());
        timeHourAnimatordialy.setValue(model.getTimeHourAlterDialy());

        useSystemWallpaper.setValue(model.isUseSystemWallpaper());

        triggerScreen.setValue(model.isTriggerScreen());
    }

    public MutableLiveData<String> getCity() {
        return mCity;
    }

    public MutableLiveData<Integer> getBackgroundColor() {
        return backgroundColor;
    }

    public MutableLiveData<Integer> getForegroundColor() {
        return foregroundColor;
    }

    public MutableLiveData<Uri> getBackgroundImageUri() {
        return backgroundImageUri;
    }

    public Boolean isTickSound() {
        return tickSound;
    }

    public MutableLiveData<String> getBackgroundAnimatorName() {
        return backgroundAnimatorName;
    }

    public Boolean getBackgroundAnimatorRandomColor() {
        return backgroundAnimatorRandomColor;
    }

    public MutableLiveData<Boolean> getSrceenLock() {
        return srceenLock;
    }

    public MutableLiveData<Integer> getBrightness() {
        return brightness;
    }

    public MutableLiveData<ClockInterfaceTypeEnum> getClockInterfaceTypeEnum() {
        return clockInterfaceTypeEnum;
    }

    public MutableLiveData<Boolean> getTriggerScreen() {
        return triggerScreen;
    }

    public void setTriggerScreen(Boolean triggerScreen) {
        this.triggerScreen.setValue(triggerScreen);
        model.setTriggerScreen(triggerScreen);
    }

    public MutableLiveData<ClockModeEnum> getClockModeEnum() {
        return clockModeEnum;
    }

    public MutableLiveData<Boolean> getBackgroundImageVisable() {
        return backgroundImageVisable;
    }

    public MutableLiveData<Boolean> getHourSystem12() {
        return hourSystem12;
    }

    public MutableLiveData<Integer> getTime_hour() {
        return time_hour;
    }

    public MutableLiveData<Date> getTime_day() {
        return time_day;
    }


    public Boolean getTickSound() {
        return tickSound;
    }

    public MutableLiveData<Integer> getBackgroundAnimatorColor() {
        return backgroundAnimatorColor;
    }

    public MutableLiveData<Integer> getHandUpTime() {
        return handUpTime;
    }

    public MutableLiveData<Boolean> getHandUpAble() {
        return handUpAble;
    }

    public MutableLiveData<Boolean> getUseSystemWallpaper() {
        return useSystemWallpaper;
    }

    public void setUseSystemWallpaper(Boolean useSystemWallpaper) {
        this.useSystemWallpaper.setValue(useSystemWallpaper);
        model.setUseSystemWallpaper(useSystemWallpaper);
    }

    public void checkSystemWallpaperSet(){
        if(useSystemWallpaper.getValue()) {
            backgroundColor.setValue(Color.TRANSPARENT);
            backgroundImageVisable.setValue(false);
        }else{
            backgroundColor.setValue(model.getBackgroundColor());
            backgroundImageVisable.setValue(model.isBackgroundImageVisable());
        }
    }

    public void setHandUpTime(Integer handUpTime) {
        this.handUpTime.setValue(handUpTime);
        //model.setHandUpTime(handUpTime);
    }


    public void setCity(String mCity) {
        this.mCity.setValue(mCity);
        model.setCity(mCity);
    }

    public Integer getScreenLockPassword() {
        return model.getScreenLockPassword();
    }

    public void setScreenLockPassword(Integer screenLockPassword) {
        model.setScreenLockPassword(screenLockPassword);
    }

    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor.setValue(backgroundColor);
        model.setBackgroundColor(backgroundColor);
    }

    public void setForegroundColor(Integer foregroundColor) {
        this.foregroundColor.setValue(foregroundColor);
        model.setForegroundColor(foregroundColor);
    }

    public void setBackgroundImageUri(Uri backgroundImageUri) {
        this.backgroundImageUri.setValue(backgroundImageUri);
        model.setBackgroundImage(backgroundImageUri.toString());
    }

    public void setBackgroundAnimatorName(String backgroundAnimatorName) {
        this.backgroundAnimatorName.setValue(backgroundAnimatorName);
        model.setBackgroundAnimatorName(backgroundAnimatorName);
    }

    public void setBackgroundAnimatorColor(Integer backgroundAnimatorColor) {
        this.backgroundAnimatorColor.setValue(backgroundAnimatorColor);
    }

    public void setBackgroundAnimatorRandomColor(Boolean backgroundAnimatorRandomColor) {
        this.backgroundAnimatorRandomColor=backgroundAnimatorRandomColor;
        model.setBackgroundAnimatorRandomColor(backgroundAnimatorRandomColor);
    }

    public void setClockInterfaceTypeEnum(ClockInterfaceTypeEnum clockInterfaceTypeEnum) {
        this.clockInterfaceTypeEnum.setValue(clockInterfaceTypeEnum);
        model.setClockInterfaceType(clockInterfaceTypeEnum.code);
    }

    public void setSrceenLock(Boolean srceenLock) {
        this.srceenLock.setValue(srceenLock);
    }

    public void setBackgroundImageVisable(Boolean backgroundImageVisable) {
        this.backgroundImageVisable.setValue(backgroundImageVisable);
        model.setBackgroundImageVisable(backgroundImageVisable);
    }

    public void setTickSound(Boolean tickSound) {
        this.tickSound = tickSound;
        model.setTickSound(tickSound);
    }

    public void setBrightness(Integer brightness) {
        this.brightness.setValue(brightness);
    }

    public void setClockModeEnum(ClockModeEnum clockModeEnum) {
        this.clockModeEnum.setValue(clockModeEnum);
    }

    public void setHourSystem12(Boolean hourSystem12) {
        this.hourSystem12.setValue(hourSystem12);
        model.setHourSystem12(hourSystem12);
    }

    public void setTime_hour(Integer time_hour) {
        this.time_hour.setValue(time_hour);
    }

    public void setTime_day(Date time_day) {
        this.time_day.setValue(time_day);
    }

    public MutableLiveData<Boolean> getAutoFullScreen() {
        return autoFullScreen;
    }

    public void setAutoFullScreen(Boolean autoFullScreen) {
        this.autoFullScreen.setValue(autoFullScreen);
        model.setAutoFullscreen(autoFullScreen);
    }


    public void setHandUpAble(Boolean handUpAble) {
        this.handUpAble.setValue(handUpAble);
        model.setHandUpAble(handUpAble);
    }

    public MutableLiveData<String> getHandUpTypeName() {
        return handUpTypeName;
    }

    public void setHandUpTypeName(String handUpTypeName) {
        this.handUpTypeName.setValue(handUpTypeName);
        model.setHandUpTypeName(handUpTypeName);
    }


    public MutableLiveData<String> getHourAlterTypeName() {
        return hourAlterTypeName;
    }

    public void setHourAlterTypeName( String hourAlterTypeName) {
        this.hourAlterTypeName.setValue(hourAlterTypeName);
        model.setTimeHourAlterTypeName(hourAlterTypeName);
    }

    public MutableLiveData<Integer> getTimeHourAnimatordialy() {
        return timeHourAnimatordialy;
    }

    public void setTimeHourAnimatordialy(Integer timeHourAnimatordialy) {
        this.timeHourAnimatordialy.setValue(timeHourAnimatordialy);
        model.setTimeHourAlterDialy(timeHourAnimatordialy);
    }

    /*public MutableLiveData<Boolean> getTimeHourAnimatorStarting() {
        return timeHourAnimatorStarting;
    }

    public void setTimeHourAnimatorStarting(Boolean timeHourAnimatorStarting) {
        this.timeHourAnimatorStarting.setValue(timeHourAnimatorStarting);
    }*/

    public Boolean isAppConfig() {
        return appConfig.getValue();
    }

    public void setAppConfig(Boolean appConfig) {
        this.appConfig.setValue(appConfig);
    }

    public MutableLiveData<Boolean> getAppConfig(){
        return appConfig;
    }

    public String getTimeHourVideoPath(){
        return model.getTimeHourVideoPath();
    }

    public boolean isFullscreenSpiritAble() {
        return model.isFullscreenSpiritAble();
    }

    public boolean isTodoSyncAble() {
        return model.isTodoSyncAble();
    }

    public void setTodoSyncAble(boolean syncAble) {
       todoSyncAble.setValue(syncAble);
       model.setTodoSyncAble(syncAble);
    }

    public MutableLiveData<Boolean> getTodoSyncAble(){
        return todoSyncAble;
    }


    public ThemeUIManager getThemeUIManager() {
        return themeUIManager;
    }

    public void setThemeUIManager(ThemeUIManager themeUIManager) {
        this.themeUIManager = themeUIManager;
    }

    public ClockStateMachine getClockStateMachine() {
        return clockStateMachine;
    }

    public void setClockStateMachine(ClockStateMachine clockStateMachine) {
        this.clockStateMachine = clockStateMachine;
    }

    public TodoSyncServiceManager getTodoSyncServiceManager() {
        return todoSyncServiceManager;
    }

    public void setTodoSyncServiceManager(TodoSyncServiceManager todoSyncServiceManager) {
        this.todoSyncServiceManager = todoSyncServiceManager;
    }
}
