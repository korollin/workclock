package clock.socoolby.com.clock.todo.microsoft.bean.todo;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.todo.microsoft.bean.AbstractChangeLogMircsoftEntity;


public class TodoFolder extends AbstractChangeLogMircsoftEntity {

    public static final String ISDEFAULTFOLDER="isDefaultFolder";
    private String isdefaultfolder;
    private String name;
    public static final String PARENTGROUPKEY="parentGroupKey";
    private String parentgroupkey;


    public void setIsdefaultfolder(String isdefaultfolder) {
        this.isdefaultfolder = isdefaultfolder;
    }
    public String getIsdefaultfolder() {
        return isdefaultfolder;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setParentgroupkey(String parentgroupkey) {
        this.parentgroupkey = parentgroupkey;
    }
    public String getParentgroupkey() {
        return parentgroupkey;
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        super.fromJson(jsonObject);

    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
